/* 
 * File:   drawHull.h
 * Author: carlchapman
 *
 * Created on November 20, 2011, 12:54 AM
 */

// **********
// drawHull.h (50 pts)
// **********

#ifndef DRAWHULL_H
#define DRAWHULL_H 

#include "convexHull.h"
#include <sys/time.h>



//this class encapsulates rgb values of different colors and allows abstraction by color names
enum colorName{BLANCHED_ALMOND,DARK_SLATE_GREY,WHITE,GREEN,CYAN,YELLOW,MEDIUM_BLUE,BLUE,RED,GREEN_YELLOW,BLACK,ORANGE,TEAL,KHAKI,INDIGO,MISTY_ROSE,OLIVE,PERU };
class Color{
public:
    double r;
    double g;
    double b; 

    Color(double red,double green,double blue){
        this->r=red;
        this->g=green;
        this->b=blue;
    }
    
    Color(colorName c=WHITE){
        D=255.0;
        switch (c){
            case BLANCHED_ALMOND:
                r=1;
                g=235/D;
                b=205/D;
                break;
            case DARK_SLATE_GREY:
                r=47/D;
                g=79/D;
                b=79/D;
                break;
            case WHITE:
                r=1;
                g=1;
                b=1;
                break;
            case GREEN:
                r=0;
                g=1;
                b=0;
                break;
            case YELLOW:
                r=1;
                g=1;
                b=0;
                break;
            case ORANGE:
                r=1;
                g=165/D;
                b=0;
                break;
            case MEDIUM_BLUE:
                r=0;
                g=0;
                b=205/D;
                break;
            case BLUE:
                r=0;
                g=0;
                b=1;
                break;
            case RED:
                r=1;
                g=0;
                b=0;
                break;
            case GREEN_YELLOW:
                r=173/D;
                g=255/D;
                b=47/D;
                break;
            case BLACK:
                r=0;
                g=0;
                b=0;
                break;
            case CYAN:
                r=0;
                g=1;
                b=1;
                break;
            case PERU:
                r=205/D;
                g=133/D;
                b=63/D;
                break;
            case TEAL:
                r=0;
                g=80/D;
                b=80/D;
                break;
            case KHAKI:
                r=240/D;
                g=230/D;
                b=140/D;
                break;
            case INDIGO:
                r=75/D;
                g=0;
                b=130/D;
                break;
            case MISTY_ROSE:
                r=255/D;
                g=228/D;
                b=225/D;
                break;
            case OLIVE:
                r=128/D;
                g=128/D;
                b=0;
                break;            
        }
    }
private:
    double D;
};



// for point display
int numPoints;   
Point points[500]; 
bool displayGrid; //this boolean controls the display of the euclidean grid of the current display
bool randomizeColors;
colorName colors[6];

extern method action;  // set to one of three values by keyboard() in proj5.cpp
// 
//    HULL_DISPLAY: display the convex hull & points
//    GRAHAM_SCAN:  animate execution of Graham's scan
//    JAVIS_MARCH:  animate execution of Javis' march
//	  TOGGLE_GRID:  toggle the displaying of the euclidean grid

// ------------
// Window Setup
// ------------

// clipping area consists of all points (x, y) where 
// xmin <= x <= xmax, ymin <= y <= ymax 
double xmin; 
double xmax; 
double ymin; 
double ymax; 

// dimensions (in pixels) of the window 
int winWidth; 
int winHeight; 

const int windowSize = 600;  // larger one of the width and height


// (5 pts) update xmin, xmax, ymin, ymax given a set of points. called by
// setDisplayRegion(). 
void updateCoordinateRanges(int n, const Point& pt);   

// (2 pts) set up the clipping volume [xmin, xmax] x [ymin, ymax].
// applies to the points stored at points[0] to points[numPoints-1]. 
void setDisplayRegion(int n, const Point pts[]); 


// set window dimensions winWidth and winHeight based on the ranges of x and y
// coordinates of the data, with the longest dimension set to windowSize.
void setWindowSize(double dx, double dy);   

// display callback function specifying the volume to be shown in the display window. 
void simpleReshape(int w, int h);          // display call back function




// -------------------------------
// Convex Hull Display & Animation
// -------------------------------



// (8 pts) 
void displayHull(colorName cName=DARK_SLATE_GREY); 
//display the line loop that forms the outer hull in the given color


// (35 pts) including subroutines called by the function 
void convexHullConstruct();


//notice that in this and the drawHull.cpp file, these are organized by function and then alphabetically
	//********************************************************************************
	//							DRAWING FUNCTIONS									//
																					//
void blinkLine(Point& p, stack<Point>& currentStack, int numTimes=2, int t1=500000, int t2=150000,colorName c1=YELLOW,colorName c2=MEDIUM_BLUE);
void clear(colorName cName=BLANCHED_ALMOND);// clears the screen to have the given background color //
void drawAllPoints();//draws all of the points in the array called 'points'			//
void drawGrid();//draws a numbered grid with 5 or fewer lines to help identify point values
void drawLineBetween(Point& a, Point& b, colorName cName=GREEN);//draws a line between two points in the given color
void drawLinesAndLabels(Point& P, int waitTime=0);//draw lines and labels indicating sorting between this and other points
void drawLinesUnder(double x);//as the ray rotates in the jm scan, this draws the lines that have been found
void drawStack(stack<Point> currentStack, colorName C1=BLUE, colorName C2=ORANGE); // draw lines between vertices on the currentStack
void showGrahamScan(colorName cName=GREEN);//show the Graham's scan initial sort by polar angle
void showJarvisScan(Point& P,stack<Point> currentStack,colorName cName = GREEN);// show one sorting in Jarvis March relative to the current point
	//********************************************************************************

	
	

	//********************************************************************************
	//						TEXT FUNCTIONS / UTILITIES								//
void displayMessage(string s,int up=10, int right=10, int pauseLength = 1000, colorName c1=BLUE,colorName c2=RED);
//displays a message up and right the given pixels from the lower left, 
//morphing from c1 to c2 and waiting 'wait' microseconds between each of the 510 steps

double dist4(double x1, double x2, double y1, double y2);//performs the classic pythagorean distance calculation between two points
void drawStringByPixel(int xPix, int yPix, string s, void* font=GLUT_BITMAP_HELVETICA_10, double r=0, double g=1, double b=0);
//draws a string by the indicated pixel with font and color selection optional

void drawStringByPoint(Point& p, string s,double xSpace=5, double ySpace=5, void* font=GLUT_BITMAP_HELVETICA_10,colorName cName=GREEN);
//draws a string by a point with some space control and font and color control optional

string intToString(int n);//converts an int to a string using stringstream
bool listContains(Point& p);//true if the point is contained in the list of vertices.
colorName randomColor();
void wait(int ms);//waits for the given number of microoseconds (approximately)
	//********************************************************************************

	
	
#endif
/* DRAWHULL_H */

