
/* 
 * File:   convexHull.cpp
 * Author: carlchapman
 *
 * Created on December 2, 2011, 15:53 PM
 */

#include "convexHull.h"


	//////////////////////////////////////////////////////////////////////////////////
	//				GIVEN FUNCTIONS FOR POINT										//
	//////////////////////////////////////////////////////////////////////////////////
	


    Point::Point(const Point& pt) : x(pt.x), y(pt.y) {
    }//copy

    Point Point::operator-() const {
        Point X(-x, -y);
        return X;
    } //negation

    Point Point::operator-(const Point &q) const {
        return Point(x - q.x, y - q.y);
    } //vector subtraction

    double Point::operator*(const Point &q) const {
        return x * q.x + y * q.y;
    } // (1 pt) dot product 

    double Point::operator%(const Point &q) const {
        return x * q.y - y * q.x;
    }// (1 pt) cross product of two vectors, one formed
    // by the origin and this point and the other formed
    // by the origin and the given point.

    double Point::norm() const {
        return dist(Point(0,0), *this);
    } // (1 pt) dist from the origin to the point 

    Point Point::unit_vector() const {
        return Point(x / norm(), y / norm());
    } // (1 pt) unit vector from the origin to
    // the point

    double Point::polarAngle(Point referencePt) const {
	
	//use the atan2 to get the +/-pi degree version
        double polarAngle = atan2(y - referencePt.y, x - referencePt.x);
		
		//convert to the 0-2pi version
        if (polarAngle < 0)
            polarAngle += 2 * pi;
			
		//make anything less than the rotation one entire turn farther away
        if (polarAngle < rotation)
            polarAngle += 2 * pi;
        return polarAngle - rotation;
    }// polar angle relative to current rotation going counter-clockwise

    void Point::draw(int size, float r, float g, float b) const {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_POINT_SMOOTH);
        glPointSize(size);
        glBegin(GL_POINTS);
        glColor3f(r, g, b);
        glVertex2f(x, y);
        glEnd();
    }// draw a point as a disk

    void Point::setXY(double xx, double yy) {
        x = xx;
        y = yy;
    } // reset the x- & y-coordinate values 
	
	
	//////////////////////////////////////////////////////////////////////////////////
	//					FUNCTIONS ADDED TO POINT									//
	//////////////////////////////////////////////////////////////////////////////////
	
	
    string Point::toString() const {
        stringstream ss;
        ss << "( " << x << ", " << y << " )";
        return ss.str();
    } // gets string representing this point
	
    bool Point::operator==(const Point& other) const {
        return fabs(this->y - other.y) < epsilon && fabs(this->x - other.x) < epsilon;
    } //the two are equal if their two points are within epsilon of being the same
	
	double Point::cross(const Point& p2, const Point& p3) {
        return (p2.x - this->x)*(p3.y - this->y)-(p2.y - this->y)*(p3.x - this->x);
        //(x_2-x_1)(y_3-y_1)-(y_2-y_1)(x_3-x_1).
    }// finds the cross product of two vectors, one formed
    // by this point and p2, and the other formed by 
    //this point and p3. A negative result is a clockwise turn,
    //a positive result is counter-clockwise and 0 is collinear

    Point  Point::getNexus() {
        return nexus;
    } // get the point used to calculate operator<

    void Point::setNexus(Point& newNexus) {
        Point::nexus = newNexus;
    } // set the point used to caluculate operator<

    double Point::getRotation() {
        return rotation;
    } // get the angle (in radians) used to offset polar angles

    void Point::setRotation(double newRotation) {
        rotation = newRotation;
    } // set the angle (in radians) used to offset polar angles



	//////////////////////////////////////////////////////////////////////////////////
	//							EXCEPTIONS											//
	//////////////////////////////////////////////////////////////////////////////////
	
	
    const char* twoPointsEqual::what() const throw () {
        string s = "the point ";
        s += p1.toString();
        s += " is the same as the point ";
        s += p2.toString();
        return s.c_str();
    }

	//////////////////////////////////////////////////////////////////////////////////
	//				CONVEX HULL BASE CLASS (ABSTRACT)								//
	//////////////////////////////////////////////////////////////////////////////////

	
    convexHull::convexHull(int numPts, Point pts[]) : numPoints(numPts), points(pts), hullConstructed(false) {
       // cout << "convex Hull constructor" << endl;
    } // constructor 

    int convexHull::getNumPoints() {
        return numPoints;
    } // gets the number of points in the hull

    void convexHull::getPoints(Point* pts) {
        int n = getNumPoints();
        pts = new Point[n];
		for(int i=0;i<n;i++)
			pts[i]=points[i];
    } // gets the points currently being used by the hull

    Point convexHull::getCurPoint() {
        return curPoint;
    } // get the point being processed

    void convexHull::getTempVertices(stack<Point>& stk) {
        stk = tempVertices;
    }//  get tempVertices for display


	//////////////////////////////////////////////////////////////////////////////////
	//				CONVEX HULL CUSTOM FUNCTIONS ADDED								//
	//////////////////////////////////////////////////////////////////////////////////

	
    bool convexHull::finished() {
        return hullConstructed;
    } // true if finished constructing
	
	list<Point>& convexHull::getVertices() {
        return vertices;
    } // gets a refference to a list of the vertices of the hull
	
    void convexHull::reset() {
        hullConstructed = false;
        while (!tempVertices.empty())
            tempVertices.pop();
    } // resets the boolean and tempVertices stack to prepare for another run

    int convexHull::getMinIndex() {
        //cout<<"getting min index..."<<endl;
        //find the point with minimum y and x values
        double minY = points[0].getY();
        int minIndex = 0;
        for (int i = 1; i < numPoints; i++) {
            //cout<<"i: "<<points[i].toString()<<endl;
            //check that the difference is not within epsilon, 
            //then check if pts[i].getY() is less than the minY
            if (fabs(points[i].getY() - minY) > epsilon && points[i].getY() < minY) {
                minY = points[i].getY();
                minIndex = i;
                //break a tie for y by choosing the minimum x value of the tied pair
            } else if (fabs(points[i].getY() - minY) < epsilon && points[i].getX() < points[minIndex].getX()) {
                minIndex = i;
            }
        }
        //cout<<"min index is:"<<minIndex<<endl;
        return minIndex;
    }


	//////////////////////////////////////////////////////////////////////////////////
	//					GIVEN GRAHAM'S SCAN	FUNCTIONS								//
	//////////////////////////////////////////////////////////////////////////////////
	
	
    GrahamScan::GrahamScan(int numPts, Point pts[]) : convexHull(numPts, pts) { // (3 pts) 
        //cout << "GrahamScan constructor" << endl;
        if (numPts < 3)
            throw badInput("Input must contain at least three points.");
        sortedList = new list<Point>;
        //cout<<"size:"<<(*sortedList).size()<<endl;

        //set the lowestPoint variable and pass that to the Point class
        int minIndex = getMinIndex();
        this->lowestPoint = pts[minIndex];

        //cout << " lowestPoint: " << lowestPoint.toString() << endl;
        //put the rest of the points into the list
        for (int i = 0; i < numPts; i++) {
            //cout << "i: " << pts[i].toString() << endl;
            if (i != minIndex) {
                // cout << "pushing point " << i << endl;
                sortedList->push_back(pts[i]);

            }
        }

        cout << "finished GS constructor" << endl << endl;
    }

    method GrahamScan::which() const {
        return GRAHAM_SCAN;
    } // (1 pt) which algorithm is used 

    void GrahamScan::constructHull() { // (16 pts) construct the convex hull without graphics
        //cout << "constructing GS hull" << endl;
        counter = 0;
        while (!hullConstructed) {
            oneOp();
        }
        
    }

    // (9 pts) execute one step of Graham's scan. this could be 
    // be a push operation or a pop operation. used for
    // graphics display. store the configuration after the
    // operation in convexHull::tempVertices.

    void GrahamScan::oneOp() {
        //cout<<"opping:"<<counter<<endl;
        if (counter == 0) {
            init();
            pointStack.push(lowestPoint);
            counter++;
            tempVertices = pointStack;
        } else if (counter < 3) {
            pointStack.push(*p3++);
            counter++;
            tempVertices = pointStack;
            //iterate through the list.
        } else if (p3 != sortedList->end()) {
            curPoint = *p3;
            //cout << "iterating...now on step " << counter++ << " point:" << curPoint.toString() << endl;
            //cout<<"here"<<endl;
            //get the top two Points (popping the top one off)
            Point p2 = pointStack.top();
            pointStack.pop();
            Point p1 = pointStack.top();

            // while the turn to the next list element is clockwise,
            // discard the p2 element and reassign the top to p2, then 
            // pop that element and assign the new top to p1

            if (fabs(p1.cross(p2, curPoint)) >= epsilon && p1.cross(p2, curPoint) < 0)
                popping = true;
                //                while (fabs(p1.cross(p2, curPoint)) >= epsilon && p1.cross(p2, curPoint) < 0) {
                //                    p2 = p1;
                //                            pointStack.pop();
                //                            p1 = pointStack.top();
                //                }
                //                pointStack.push(p2);
                // pointStack.push(curPoint);

                //if the two points are collinear, add the farther point
            else if (fabs(p1.cross(p2, curPoint)) < epsilon) {
                popping = false;
                //if the points are on the same line and are the same dist and
                //are always in order from farther to closer to the nexus, 
                //then these points must be equal or close enough for epsilon
                if (fabs(dist(p1, p2) - dist(p1, curPoint)) < epsilon)
                    throw twoPointsEqual(p2, curPoint);
                    //otherwise, add the farther point
                else

                    if (dist(p1, p2) > dist(p1, curPoint))
                    pointStack.push(p2);
                else
                    pointStack.push(curPoint);
                //if the turn to the next element is counter-clockwise,
                //push both back onto the stack
            } else {

                popping = false;
                pointStack.push(p2);
                pointStack.push(curPoint);
            }
            tempVertices = pointStack;
            if (!popping)
                p3++;
        } else
            complete();
    }
	//////////////////////////////////////////////////////////////////////////////////
	//					FUNCTIONS ADDED TO GRAHAM'S SCAN							//
	//////////////////////////////////////////////////////////////////////////////////
		
    GrahamScan::~GrahamScan() {
        delete sortedList;
    }

    Point& GrahamScan::getLowestPoint() {
        return lowestPoint;
    }

    list<Point>& GrahamScan::getSortedList() {
        return *sortedList;
    }


    void GrahamScan::init() {
        hullConstructed = false;
        Point::setNexus(lowestPoint);
        Point::setRotation(0.0);
        sortedList->sort();
        popping = false;
        p3 = sortedList->begin();
    }
    //put the elements on the final list
	

   void GrahamScan::complete() {
        vertices.clear();
        while (!pointStack.empty()) {
            vertices.push_front(pointStack.top());
            pointStack.pop();
        }
        numVertices = vertices.size();
        hullConstructed = true;
        //cout << "hull constructed" << endl;
        counter = 0;
        //cout << "counter reset to zero" << counter << endl;
    }




	//////////////////////////////////////////////////////////////////////////////////
	//					GIVEN JARVIS MARCH FUNCTIONS								//
	//////////////////////////////////////////////////////////////////////////////////

    JarvisMarch::JarvisMarch(int numPts, Point pts[]) : convexHull(numPts, pts) { //  (3 pts) 
        //cout << "JarvisMarch constructor" << endl;
        if (numPts < 3) {
            throw badInput("Input for Jarvis' March must contain at least three points.");
        }
        unprocessedList = new list<Point>;

        //set the lowestPoint variable and pass that to the Point class
        int minIndex = getMinIndex();
        this->firstPoint = pts[minIndex];
        //cout << "firstPoint:" << firstPoint.toString()<< endl;

        //put the rest of the points into the list and then sort by polar angle
        for (int i = 0; i < numPts; i++) {
            //if(i!=minIndex)
            unprocessedList->push_back(pts[i]);
        }
        //cout << "finished JM constructor" << endl << endl;
    }

    method JarvisMarch::which() const {

        return JARVIS_MARCH;
    } // (1 pt) which algorithm is used 

    void JarvisMarch::constructHull() { // (13 pts) run Javis' march algorithm with no dealing with
        // graphics 
        //cout << "constructing hull" << endl;
        //cout << "firstPoint:" << firstPoint.toString() << "nexus:" << Point::getNexus().toString() << endl;
        while (!hullConstructed) {
            oneOp();
        }
        complete();
    }

    void JarvisMarch::oneOp() {// (9 pts) execute one step which is to find the next vertex. 
        
        if (stepCounter == 0) {
            init();
        } else
            getNextVertex();


    }

	//////////////////////////////////////////////////////////////////////////////////
	//					CUSTOM JARVIS MARCH FUNCTIONS								//
	//////////////////////////////////////////////////////////////////////////////////
	
    JarvisMarch::~JarvisMarch() {
        delete unprocessedList;
    }
	
    list<Point>& JarvisMarch::getSortedList() {
        return *unprocessedList;
    }

    Point& JarvisMarch::getNextPoint() {
        list<Point>::iterator it = unprocessedList->begin();
        if ((*it) == tempVertices.top())
            it++;
        return *it;
    }
	
    void JarvisMarch::init() {
        //cout << "initializing jm" << endl;
        //stepCounter = 0;
        Point::setNexus(firstPoint);
        Point::setRotation(0.0);
        //        cout << "list before sorting" << endl;
        //        list<Point>::iterator p3;
        //        int counter;
        //        counter = 0;
        //        p3 = unprocessedList->begin();
        //        while (p3 != unprocessedList->end()) {
        //            Point n = *p3;
        //            cout << "point#" << counter << n.toString() << endl;
        //            cout << "polar angle:" << n.polarAngle(Point::getNexus()) << endl;
        //            ;
        //            counter++;
        //            p3++;
        //        }
        unprocessedList->sort();
        //        cout << "list after sorting" << endl;
        //        counter = 0;
        //        p3 = unprocessedList->begin();
        //        while (p3 != unprocessedList->end()) {
        //            Point n = *p3;
        //            cout << "point#" << counter << n.toString() << endl;
        //            cout << "polar angle:" << n.polarAngle(Point::getNexus()) << endl;
        //            counter++;
        //            p3++;
        //        }
        curPoint = firstPoint;
        tempVertices.push(firstPoint);
        stepCounter++;
        //cout << "stepCounter incremented:" << stepCounter << endl;
    }

    void JarvisMarch::complete() {
        //cout << "completing jm" << endl;
        vertices.clear();
        while (!tempVertices.empty()) {
            Point topOfStack = tempVertices.top();
            //cout << "top of stack is: " << topOfStack.toString() << endl;
            vertices.push_front(topOfStack);
            tempVertices.pop();
        }
        numVertices = vertices.size();
        //cout << "completed" << endl;
        stepCounter = 0;
        //cout << "stepCounter reset:" << stepCounter << endl;
    }

    void JarvisMarch::getNextVertex() {
        if (!hullConstructed) {
            Point nextVertex = getNextPoint();
            //cout << endl << "nextVertex:" << nextVertex.toString() << endl;
            //cout<<"still constructing"<<endl;
            double set = nextVertex.polarAngle(curPoint) + Point::getRotation();
            //cout << "setting the rotation:" << set << " with current point: " << curPoint.toString() << endl;
            Point::setRotation(set);
            curPoint = nextVertex;
            tempVertices.push(nextVertex);
            Point::setNexus(nextVertex);
            //unprocessedList->pop_front();
            //            cout<<"firstPoint:"<<firstPoint.toString()<<"nexus:"<<Point::getNexus().toString()<<endl;
            //            cout<<"list before sorting"<<endl;
            list<Point>::iterator p3;
            int counter;
            //            counter=0;
            //            p3=unprocessedList->begin();
            //            while(p3!=unprocessedList->end()){
            //                Point n = *p3;
            //                cout<<"point#"<<counter<<n.toString()<<endl;
            //                cout<<"polar angle:"<<n.polarAngle(Point::getNexus())<<endl;;
            //                counter++;
            //                p3++;
            //            }
            (*unprocessedList).sort();
            //        cout << "list after sorting" << endl;
            //        counter = 0;
            //        p3 = unprocessedList->begin();
            //        while (p3 != unprocessedList->end()) {
            //            Point n = *p3;
            //            cout << "point#" << counter << n.toString() << endl;
            //            cout << "polar angle:" << n.polarAngle(Point::getNexus()) << endl;
            //            counter++;
            //            p3++;
            //        }

            stepCounter++;
            //cout << "stepCounter incremented" << stepCounter << endl;
            //cout << "firstPoint:" << firstPoint.toString() << " nextPoint: " << getNextPoint().toString() << endl;
            if (getNextPoint() == firstPoint) {
                //cout<<"hull constructed"<<endl;
                hullConstructed = true;
            }


        } else {
            complete();
        }
    }

