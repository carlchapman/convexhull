
// *********
// proj5.cpp (25 pts)
// *********


#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <GLUT/glut.h>
#include "convexHull.h"


using namespace std;

//initialize the static variables in the Point class
Point Point::nexus=Point();
double Point::rotation=0.0;

// ------------------------------
// externs declared in drawHull.h
// ------------------------------
//
extern int numPoints;
extern Point points[500];
extern bool displayGrid;

// coordinate range of the displayed region
extern double xmin;
extern double xmax;
extern double ymin;
extern double ymax;

// window size
extern int winWidth;
extern int winHeight;

void keyboard(unsigned char key, int x, int y);
void setDisplayRegion(int n, const Point pts[]);
void setWindowSize(double dx, double dy);
void simpleReshape(int w, int h);
void convexHullConstruct();

//********************************************************************
double dist4(double x1, double x2, double y1, double y2);			//
double getDoubleFrom(string s);										//
bool isNumerical(char c);											//
//********************************************************************

template <class T, int size>
T min(const T (&r_array)[size])
{
     /* parameterized function for finding minimum 
        value contained in an array */ 
    static T x=x+size;
     T min_val = r_array[0]; 
     int i;
     for (i = 0; i < size; ++i)
           if (r_array[i] < min_val)
                 min_val = r_array[i]; 
     cout<<"x:"<<x<<endl;
     return min_val; 
}

// -----------------------------------
// hull objects passed to drawHull.cpp
// -----------------------------------
convexHull *cHull;
GrahamScan *gsHull;
JarvisMarch *jmHull;
//
//// key value 
method action;

//********************************************************************
//   Friend functions 												//
//these need to be defined outside their friend classes but not twice
// which happens if defined in any other suitable location			//
																	//
ostream& operator<<(ostream& ostr, convexHull* ch) {				//
    if (!ch->finished()) {											//
        ch->constructHull();										//
    }																//
    list<Point>::iterator it = (*ch).getVertices().begin();			//
    list<Point>::iterator last = (*ch).getVertices().end();			//
    int counter = 0;												//
    while (it != last) {											//
        if (counter % 5 == 0 || it == last) {						//
            ostr << endl;											//
        } else {													//
            ostr << " ";											//
        }															//
        ostr << (*it).toString();									//
        counter++;													//
        it++;														//
    }																//
    return ostr;													//
}// the out stream operator for convex hulls						//
// a friend function of convex hull									//
																	//
double dist(const Point& a, const Point& b) {						//
    return dist4(a.x, b.x, a.y, b.y);								//
} // the distance between two points								//
// a friend function of Point										//
																	//
bool operator<(Point& first, Point& second) {						//
    //find both polar angles relative to the current nexus			//
    double p1 = first.polarAngle(Point::nexus);						//
    double p2 = second.polarAngle(Point::nexus);					//
																	//
    //if the two angles are equal or very close to equal, 			//
    //choose the farthest point										//
    if (fabs(p1 - p2) < epsilon) {									//
        double d1 = dist(Point::nexus, first);						//
        double d2 = dist(Point::nexus, second);						//
        //if the points are equal, throw an exception				//
        if (fabs(d1 - d2) < epsilon)								//
            throw twoPointsEqual(first, second);					//
        else														//
            return d1>d2;											//
    } else															//
        return p1<p2;												//
}// comparison for list sorting										//
// a friend function of Point										//
//********************************************************************




// (20 pts)
int main(int argc, char** argv) {

    //    1. read in a file name
    //cout << "Please enter a file name (must be stored in the same folder as this file)." << endl;
    string filename="randomSquare.txt";
    displayGrid=true;
    //cin >> filename;
    char word[200];
    string s;
 
    //    2. read point data from the file into points[], and set numPts. 
    ifstream ifs(filename.c_str(), ifstream::in);
    bool first=true;
    double xValue=0.0;

    while (ifs.good()) {
	ifs.getline(word, 199, ' ');
	if (word[0] != ' ')
	    s=word;
	if(s.size()>0&&s[s.size()-1]!='\n'){
//	    cout<<"size: "<<s.size()<<endl;
//	    cout<<"first:"<<(int)s[0]<<endl;
//	    cout<<"value:"<<(int)s[s.size()-1]<<endl;
	    double d=getDoubleFrom(s);
//	    cout<<d<<endl;
	    if(first){
		first=false;
		xValue=d;
	    }else{
		first=true;
		points[numPoints++]= Point(xValue,d);
	    }
	}
    }
    ifs.close();
    cout<<"numPoints:"<<numPoints<<endl;
    for(int i=0;i<numPoints;i++){
	cout<<points[i].toString()<<endl;
    }
    // create two dynamic objects of derived classes GrahamScan and
    // JarvisMarch, respectively, and let them be pointed at by gsHull and
    // jmHull. Note that the two objects represent the same point set. 

    convexHull * cHull[2];
    cout<<"creating objects"<<endl;
    cHull[0] = gsHull= new GrahamScan(numPoints,points);
    cHull[1] = jmHull= new JarvisMarch(numPoints,points);
    // size not specified -- ok
int ia[ ] = { 10, 7, 14, 3, 25 }; 
// size = # values in the initializer list
double da[ ] = { 10.2, 7.1, 14.5, 3.2, 25.0, 16.8 };
cout<<"ai"<<min(ia)<<endl;
cout<<"ai"<<min(ia)<<endl;
cout<<"ai"<<min(ia)<<endl;
cout<<"ai"<<min(ia)<<endl;
cout<<"da"<<min(da)<<endl;
//
//cout
//    // (10 pts) if correct results
    int i;
    for (i = 0; i < 2; i++) {
	//cout<<"i:"<<i<<endl;
	cHull[i]->constructHull();
	cout << cHull[i] << endl << endl;
	cHull[i]->reset();
    }
    setDisplayRegion(numPoints, points); //adjusts y&xmin/max 
    setWindowSize(xmax - xmin, ymax - ymin);
    cout<<"xmax: "<<xmax<<" xmin: "<<xmin<<" ymax: "<<ymax<<" ymin: "<<ymin<<" winwidth: "<<winWidth<<"winheight"<<winHeight<<endl;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(winWidth, winHeight); // width and height of the window in pixels. 
    glutInitWindowPosition(0, 0); // position (in pixels) of the window's upper left corner on the computer screen
    glutCreateWindow("Convex Hull Construction");
    glutDisplayFunc(convexHullConstruct);
    glutReshapeFunc(simpleReshape);
    glutKeyboardFunc(keyboard);
    glutMainLoop();

    delete gsHull;
    delete jmHull;

    return 0;
}



// (5 pts) enter 
// 
//    '1' for displaying convex hull and points 
//    '2' for animating Graham's scan 
//    '3' for animating Javis' march
//    '4' for toggling the grid
//    'q' for exit 
// 
// set the value of action accordingly 

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
    case 'q':
	exit(0);
	break;
    case '1': // display the convex hull and all points 
	// ... 
	action=HULL_DISPLAY;
	break;
    case '2': // animate execution of Graham's scan 
	action=GRAHAM_SCAN;
	// ...           
	break;
    case '3': // animate execution of Javis' march
	action= JARVIS_MARCH;
	break;
    case '4': // toggle the grid
	action= TOGGLE_GRID;
	break;
    case '5': // toggle the grid
	action= TOGGLE_RANDOM_COLOR;
	break;
    }
    glutPostRedisplay();
}

//gets a double value from a string
double getDoubleFrom(string s){
    int start=0;
    int end=0;
    int last=s.size()-1;
    bool found=false;
    for(int i=0;i<=last;i++){
	char c=s[i];
	
	if(found&&!isNumerical(c)){
	    end=i;
	    break;
	}else if(!found&&isNumerical(c)){
	    start=i;
	    found=true;
	}
    }
    s=s.substr(start,end-start);
    return atof(s.c_str());
}

//true if the character is a numerical character including '.' and '-'
bool isNumerical(char c){
    int n = (int)c;
    return (n>47&&n<58)||c=='-'||c=='.';
}





