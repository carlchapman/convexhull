
#include <iostream>
#include <list>

using namespace std;

class B {
public:

    B(double m) {
        this->n = m;
    }

    double static getR() {
        return R;
    }

    void static setR(double newR) {
        B::R = newR;
    }

    double getN() {
        return n;
    }

private:
    double n;
    static double R;
};
double B::R=0;
bool compare(B l, B r);


int main() {
    list<B> L;
    L.push_front(1.0);
    L.push_front(22.0);
    L.push_front(-11.0);
    L.push_front(4.0);
    B b(9.0);
    b.setR(11.0);
    L.sort(compare);
    list<B>::iterator it = L.begin();
    while (it != L.end()) {
        cout << (*it).getN() << endl;
        it++;
    }
    b.setR(11.0);
    L.sort(compare);
    it = L.begin();
    while (it != L.end()) {
        cout << (*it).getN() << endl;
        it++;
    }
    return 0;
}
bool compare(B l, B r) {
    return l.getN() * l.getR() > r.getN();
}

