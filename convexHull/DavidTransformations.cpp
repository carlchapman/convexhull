// Copyright (c) David Kabala 
// Fall 2009

#include <GLUT/glut.h>
#include <math.h>

class SceneManager
{
protected:
    unsigned int _Example;

public:
    SceneManager(void) : _Example(1)
    {
    }

    void init(void)
    {
    }

    void reshape(int w, int h)
    {
        glViewport(0,0,w,h);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-w/2.0,w/2.0,-h/2.0,h/2.0,-1.0,1.0);
        //glMatrixMode(GL_MODELVIEW);
        glutPostRedisplay();
    }

    void drawTranslateExample(void) const
    {
        //Clear the Draw Buffer
        glClear(GL_COLOR_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		//White box
		drawBox(1.0,1.0,1.0);

		//Translate 175 on x axis
		glTranslatef(175.0,0.0,0.0);

		//Red box
		drawBox(1.0,0.0,0.0);

		//Translate -225 on y axis
		glTranslatef(0.0,-225.0,0.0);

		//Red box
		drawBox(0.0,1.0,0.0);

        glFlush();  /* Single buffered, so needs a flush. */
    }


    void drawRotateExample(void) const
    {
        //Clear the Draw Buffer
        glClear(GL_COLOR_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		//White box
		drawBox(1.0,1.0,1.0);

		//Rotate around z axis axis
		glRotatef(45.0,0.0,0.0,1.0);

		//Red box
		drawBox(1.0,0.0,0.0);
		
        glFlush();  /* Single buffered, so needs a flush. */
    }

    void drawScaleExample(void) const
    {
        //Clear the Draw Buffer
        glClear(GL_COLOR_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		//White box
		drawBox(1.0,1.0,1.0);

		//Scaling
		glScalef(0.5,2.0,1.0);

		//Red box
		drawBox(1.0,0.0,0.0);
		
        glFlush();  /* Single buffered, so needs a flush. */
    }

    void drawExample(void) const
    {
        //Clear the Draw Buffer
        glClear(GL_COLOR_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		//White box
		drawBox(1.0,1.0,1.0);
		
		glPushMatrix();
			//Translate
			glTranslatef(200.0,0.0,0.0);

			//Red box
			drawBox(1.0,0.0,0.0);
		glPopMatrix();
		
		//Translate
		glTranslatef(400.0,0.0,0.0);
		
		//Rotate around z axis axis
		glRotatef(45.0,0.0,0.0,1.0);

		//Green box
		drawBox(0.0,1.0,0.0);
		
        glFlush();  /* Single buffered, so needs a flush. */
    }
	
    void drawStackCombination(void) const
    {
        //Clear the Draw Buffer
        glClear(GL_COLOR_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		//White box
		drawBox(1.0,1.0,1.0);
		
		glPushMatrix();
			//Translate
			glTranslatef(200.0,0.0,0.0);

			//Red box
			drawBox(1.0,0.0,0.0);
		glPopMatrix();
		
		glPushMatrix();
			//Translate
			glTranslatef(0.0,-200.0,0.0);

			//Scale
			glScalef(0.5,0.25,1.0);

			//Green box
			drawBox(0.0,1.0,0.0);
		glPopMatrix();
		
        glFlush();  /* Single buffered, so needs a flush. */
	}

	void drawBox(GLfloat red, GLfloat green, GLfloat blue) const
	{
        //Draw  a single quad
        glBegin(GL_QUADS);
            glColor3f(red,green,blue);  /* white */
            glVertex2i(-75, -75);
            glVertex2i(75, -75);
            glVertex2i(75, 75);
            glVertex2i(-75, 75);
        glEnd();
	}

    void display(void) const
    {
        switch(_Example)
        {
			case 2:
				drawRotateExample();
				break;
			case 3:
				drawScaleExample();
				break;
			case 4:
				drawStackCombination();
				break;
			case 5:
				drawExample();
				break;
			case 1:
			default:
				drawTranslateExample();
				break;
        }
    }
    
    void keyboard(unsigned char key, int x, int y)
    {
        switch(key)
        {
        case 'q':
            //exit(0);
            break;
        case '1':
            _Example = 1;
            break;
        case '2':
            _Example = 2;
            break;
        case '3':
            _Example = 3;
            break;
        case '4':
            _Example = 4;
            break;
        case '5':
            _Example = 5;
            break;
        }
        glutPostRedisplay();
    }
};

SceneManager mgr;

void reshape(int w, int h)
{
    mgr.reshape(w,h);
}

void display(void)
{
    mgr.display();
}
void keyboard(unsigned char key, int x, int y)
{
    mgr.keyboard(key,x,y);
}

int
main(int argc, char **argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
  glutCreateWindow("Transformation Examples");
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutReshapeWindow(800,640);
  glutPositionWindow(50,50);
  glutKeyboardFunc(keyboard);
  mgr.init();
  glutMainLoop();
  return 0;             /* ANSI C requires main to return int. */
}


