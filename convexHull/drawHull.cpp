// ************
// drawHull.cpp
// ************


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <GLUT/glut.h>
#include "drawHull.h"
#include <time.h>

// ----------------------
// externs from proj5.cpp
// ----------------------
extern convexHull *cHull;
extern GrahamScan *gsHull;
extern JarvisMarch *jmHull;
extern method action;


	//////////////////////////////////////////////////////////////////////////////////
	//								WINDOW SETUP									//
	//////////////////////////////////////////////////////////////////////////////////

	
// update xmin, xmax, ymin, ymax from a set of points . called by setDisplayRegion.
void updateCoordinateRanges(int n, const Point& pt) {
    double x = pt.getX();
    double y = pt.getY();
    if (x < xmin)
	xmin = x;
    if (x > xmax)
	xmax = x;
    if (y < ymin)
	ymin = y;
    if (y > ymax)
	ymax = y;
}

// set up the clipping volume [xmin, xmax] x [ymin, ymax].
void setDisplayRegion(int n, const Point pts[]) {
    xmin = xmax = ymin = ymax = 0; // include the origin in the display
    for (int i = 0; i < n; i++)
	updateCoordinateRanges(n, pts[i]);

    // include a border in the display window
    double border = 0.1 * (xmax - xmin > ymax - ymin ? xmax - xmin : ymax - ymin);
    xmin -= border;
    xmax += border;
    ymin -= border;
    ymax += border;
}

// set window dimensions based on the ranges of x and y coordinates of the data, 
// with the longest dimension set to windowSize.
void setWindowSize(double dx, double dy) {
    if (dx > dy) {
	winWidth = windowSize;
	winHeight = winWidth * dy / dx; //this is where the model informs the view of its proportions
    } else {
	winHeight = windowSize;
	winWidth = winHeight * dx / dy;
    }
}

// display callback function specifying the volume to be shown in the display window. 
void simpleReshape(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(xmin, xmax, ymin, ymax, -1, 1); // here the model is used to control the projection volume
}


	//////////////////////////////////////////////////////////////////////////////////
	//					 	CONVEX HULL ANIMATION									//
	//////////////////////////////////////////////////////////////////////////////////
void drawX(){
    clear();
//        glViewport(0, 0, winWidth, winHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-20,20,-20,20,-1,1);
    	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	//glScalef(0.4,0.4,0.4);
	drawAllPoints();


//	glMatrixMode(GL_PROJECTION);
//	glPushMatrix();//save teh old stuff
//	glLoadIdentity();
//	gluOrtho2D(0,winWidth, 0,winHeight);
//	glMatrixMode(GL_MODELVIEW);//save someting
//	glPushMatrix();
//	glLoadIdentity();
//	glScalef(0.5,0.5,1);
//	for(double i=0;i<100;i++){
//	Point pqr=Point(i,i);
//	pqr.draw(10,1,1,0.5); 
//	string s = intToString(i);
//	drawStringByPoint(pqr,s);
//	}
//	glMatrixMode(GL_MODELVIEW);
//	glPopMatrix();
//	glMatrixMode(GL_PROJECTION);
//	glPopMatrix();
	
	
	cout<<"done"<<endl;
}

// change the graphics depending on the user input indicated by 'action'
void convexHullConstruct() {

    //clear();
    switch (action) {
    case HULL_DISPLAY:
	displayHull(BLUE);
	break;
    case GRAHAM_SCAN:
	cHull = gsHull;
	break;
    case JARVIS_MARCH:
	cHull = jmHull;
	break;
    case TOGGLE_GRID:
	//zoom out

	cout<<"drawing"<<endl;
		drawX();
//	displayGrid = !displayGrid;
//	displayHull();
	break;
    case TOGGLE_RANDOM_COLOR:
	randomizeColors = !randomizeColors;
	
	//get 6 unique colors into the array
	colorName newColor=randomColor();
	colors[0]=newColor;
	for(int i=1;i<6;i++){
	    bool Unique=true;
	    do{
		Unique=true;
		newColor=randomColor();
		 for(int j=i-1;j>=0;j--){
		     Unique = Unique&&(newColor!=colors[j]);
		 }
		//cout<<"looping...i:"<<i<<endl;
	    }while(!Unique);
	    
	    colors[i]=newColor;
	}
	//for(int i=0;i<6;i++)
	  //  cout<<"colors["<<i<<"]: "<<colors[i]<<endl;
	    
	displayHull();
	break;
    }

    if (action == GRAHAM_SCAN || action == JARVIS_MARCH) {
		stack<Point> currentStack;//helper stack
		
		//display the initial sort of gs
		if (action == GRAHAM_SCAN) {
			showGrahamScan();
		}
		
		// go through the algorithm one step at a time until finished
		while (!(cHull->finished())) {
		
			//at the start, display just the points for every step
			clear();
			drawAllPoints();
			//cout << "not finished" << endl;
			
			//increment the operation, put into stack and draw
			cHull->oneOp();
			cHull->getTempVertices(currentStack);
			drawStack(currentStack);
			
			//show the sorting of js relative to the current point
			if (action == JARVIS_MARCH) {
				Point p = cHull->getCurPoint();
				showJarvisScan(p, currentStack);
				clear();
				drawAllPoints();
				drawLinesAndLabels(p,80000);
				glutSwapBuffers();
				string s = "Points sorted relative to: ";
				s += p.toString();
				displayMessage(s,10,10,800,GREEN_YELLOW);
				//cout << "gunna blink that" << endl;
				
				//blink the new line added for emphasis
				blinkLine(p, currentStack,8,100000,30000,RED,YELLOW);
				//drawStack(currentStack);
				
			//for gs, just wait between pushes and pops
			} else
				wait(250000);
		}
		//cout << "finishing" << endl;
		
		//once the hull is constructed in the object, display as a line loop
		clear();
		drawAllPoints();
		cHull->getTempVertices(currentStack);
		colorName lastColor = DARK_SLATE_GREY;
		if(randomizeColors)
		    lastColor = colors[2];
		Color c(lastColor);
		glColor3f(c.r, c.g, c.b);
		glBegin(GL_LINE_LOOP);
		double X;
		double Y;
		while (!currentStack.empty()) {
			Point t = currentStack.top();
			X = t.getX();
			Y = t.getY();
			glVertex2f(X, Y);
			currentStack.pop();
		}
		glEnd();
		
		//call the hull one more time to reset all the variables ??????????????????????????(combine with complete?)
		cHull->oneOp();
		cHull->reset();
	}
	glutSwapBuffers();

}

//display the line loop that forms the outer hull in the given color
void displayHull(colorName cName) {

	//set the current cHull to gsHull to get the correct info, check for error case
    cHull = gsHull;
    if (cHull == 0) {
		cerr << "trying to display outer hull lines - the hull is not initialized";
    }

	//get the vertices of the hull and check for error cases
    list<Point> V = cHull->getVertices();
    if (V.size() == 0) {
		cerr << "the list of vertices is empty" << endl;
    } else {

	//if the list is full, draw the points and the line loop
	clear();
	drawAllPoints();
	list<Point>::iterator it = V.begin();
	list<Point>::iterator end = V.end();
	if(randomizeColors)
		cName = randomColor();
	Color c(cName);
	glBegin(GL_LINE_LOOP);
		double X;
		double Y;
		glColor3f(c.r, c.g, c.b);
		while (it != end) {
			X = (*it).getX();
			Y = (*it).getY();
			glVertex2f(X, Y);
			it++;
		}
	glEnd();
    }
}


	//////////////////////////////////////////////////////////////////////////////////
	//							DRAWING FUNCTIONS									//
	//////////////////////////////////////////////////////////////////////////////////
	
	
//blink the line between the current point and the next point in jm
void blinkLine(Point& p, stack<Point>& currentStack, int numTimes, int t1, int t2, colorName c1, colorName c2) {

	//get the next point to be added to the vertices list
    Point q = jmHull->getNextPoint();
    if(randomizeColors){
	c1=colors[1];
	c2=colors[2];
    }
	
    Color C1(c1);
    Color C2(c2);
	
	//change color from c1 to c2 'numTimes' times, waiting for t1 and t2 between changes.
	//notice that a ratio t1:t2 being 3:1 or maybe 4:1 makes the blinking more dramatic
    for (int i = 0; i < numTimes; i++) {
		//clear();
		//drawLinesAndLabels(p);
		glColor3f(C1.r, C1.g, C1.b);
		glBegin(GL_LINES);
			glVertex2f(p.getX(), p.getY());
			glVertex2f(q.getX(), q.getY());
		glEnd();
		drawStack(currentStack);
		drawAllPoints();
		glutSwapBuffers();
		wait(t1);
		//clear();
		//drawLinesAndLabels(p);
		glColor3f(C2.r, C2.g, C2.b);
		glBegin(GL_LINES);
			glVertex2f(p.getX(), p.getY());
			glVertex2f(q.getX(), q.getY());
		glEnd();
		drawStack(currentStack);
		drawAllPoints();
		glutSwapBuffers();
		wait(t2);
    }

}

//clears the window the the given background color
void clear(colorName cName) {
    if(randomizeColors)
	cName = colors[0];
    Color c(cName);
    glClearColor(c.r, c.g, c.b, 1);
    glClearDepth(1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
	
//draws all of the points in the array called 'points' and an optional grid
void drawAllPoints() {
    colorName pointColor=RED;
    if(randomizeColors)
	pointColor=colors[1];
    Color c(pointColor);
    
    for (int i = 0; i < numPoints; i++) {
		points[i].draw(5,c.r,c.g,c.b);
	//cout<<"point "<<i<<" "<<points[i].toString()<<endl;
    }
    if (displayGrid)//also display grid if indicated
		drawGrid();
}

//draws a numbered grid with 5 or fewer lines (both x and y) to help identify point values
void drawGrid() {

	//find a factor of the model size and get an int 'Xn' 
	//that will be be zero at some point when incremented by the factor or 0
    int Xn = (int) xmin;
    int Xx = (int) xmax;
    int span = Xx - Xn;
    //cout << "span:" << span << endl;
    int factor = span / 4;
    if (factor == 0)factor = 1;
    //cout << "factor: " << factor << endl;
    Xn -= Xn % factor;
    
    //for random colors...
    colorName axisColor=BLUE,otherColor=CYAN;
        if(randomizeColors){
	axisColor=colors[2];
	otherColor=colors[3];
	}
    Color c1(axisColor);
    Color c2(otherColor);


	//draw lines and labels with the axis in blue and the rest in cyan
    for (int i = Xn; i < Xx; i += factor) {
	i == 0 ? glColor3f(c1.r,c1.g, c1.b) : glColor3f(c2.r, c2.g, c2.b);
	glBegin(GL_LINES);
		glVertex2f(i, ymax);
		glVertex2f(i, ymin);
		glVertex2f(xmin, i);
		glVertex2f(xmax, i);
	glEnd();
	Point xAxisLabel(i, 0);
	Point yAxisLabel(0, i);
	string s = intToString(i);
	drawStringByPoint(xAxisLabel, s, 2, 2);
	drawStringByPoint(yAxisLabel, s, 2, 2);
    }
}	

//draws a line between the two points in the given color or GREEN by default
void drawLineBetween(Point& a, Point& b, colorName cName) {
     if(randomizeColors){
	cName=colors[5];
     }
	Color c(cName);
    glColor3f(c.r, c.g, c.b);
    glBegin(GL_LINES);
		glVertex2f(a.getX(), a.getY());
		glVertex2f(b.getX(), b.getY());
    glEnd();
}

//draws lines from the given point to every other point in sorted order
//and puts labels by each point, pausing between each drawing for 'waitTime'
void drawLinesAndLabels(Point& P, int waitTime) {
    list<Point> sortedList = cHull->getSortedList();
    list<Point>::iterator it = sortedList.begin();
    list<Point>::iterator end = sortedList.end();
    //cout << "printing list of size: " << sortedList.size() << endl;
    //label the order of the sorted points
    int counter = 0;
    while (it != end) {
	  	string s = "p";
		drawLineBetween(P, (*it));
		s += intToString(counter++);
		drawStringByPoint(*it, s, 3, 3);
		//cout << "point" << counter << endl;
		it++;
		glutSwapBuffers();
		wait(waitTime);  
    }
}



//draws the contents of the currentStack as lines one by one,
//and for Graham's scan, invalid points are drawn in dotted lines
void drawStack(stack<Point> currentStack, colorName C1, colorName C2) {
    
    if(randomizeColors){
	C1=colors[2];
	C2=colors[4];
    }
    Color c1(C1);
    Color c2(C2);
    //cout<<"c1r: "<<c1.r<<" c1g: "<<c1.g<<" c1b: "<<c1.b<<" c2r: "<<c2.r<<" c2g: "<<c2.g<<" c2b: "<<c2.b<<endl;
    glColor3f(c1.r, c1.g, c1.b);
    //cout<<"stack empty?"<<currentStack.empty()<<endl;
    Point r = currentStack.top();
    currentStack.pop();
    while (!currentStack.empty()) {
	//cout<<"c1r: "<<c1.r<<endl;
	Point t = currentStack.top();
		if (action == GRAHAM_SCAN && !listContains(r)) {
		    //cout<<"here"<<endl;
			glEnable(GL_LINE_STIPPLE);
			glLineStipple(7, 0xFFFF);
			glColor3f(c2.r, c2.g, c2.b);
			glBegin(GL_LINES);
				
				glVertex2f(r.getX(), r.getY());
				glVertex2f(t.getX(), t.getY());
			glEnd();
			glLineStipple(7, 0x2222);
			glColor3f(c1.r, c1.g, c1.b);
			glBegin(GL_LINES);
				glVertex2f(r.getX(), r.getY());
				glVertex2f(t.getX(), t.getY());
			glEnd();
			glDisable(GL_LINE_STIPPLE);

		}else{
		glColor3f(c1.r, c1.g, c1.b);
		glBegin(GL_LINES);
			glVertex2f(r.getX(), r.getY());
			glVertex2f(t.getX(), t.getY());
		glEnd();
		}
		currentStack.pop();
		r = t;
    }
    glutSwapBuffers();
}

//animate sorting of all points by the polar anglar using a rotating ray in the
//case of Graham's scan. highlight the lowest point. 
void showGrahamScan(colorName cName){

	//highlight the lowest point
    drawAllPoints();
	//cout << "animating graham scan..." << endl;
    Point L = gsHull->getLowestPoint();
    colorName g = CYAN;
    if(randomizeColors){
	g=colors[3];
	cName=colors[5];
    }
    Color G(g);
    L.draw(10, G.r,G.g,G.b);
    drawStringByPoint(L, "lowest point", 6, -3);

    //make the ray long enough to span the longest diagonal of the window
    double x = 0;//start the angle at 0 (so ray is parellel to x axis)
    double px = L.getX();
    double py = L.getY();
    double xEnd = dist4(0, xmax - xmin, 0, ymax - ymin);
    double yEnd = 0;
	
	Color c(cName);
	    while (x < 180) {
			glColor3f(c.r, c.b, c.g);
			clear();
			//shift origin to point and rotate then draw the line
			glTranslatef(px, py, 0);
			glRotatef(x, 0, 0, 1);
			glBegin(GL_LINES);
				glVertex2f(0, 0);
				glVertex2f(xEnd, yEnd);
			glEnd();
			
			//rotate and shift back then redraw all else
			glRotatef(-x, 0, 0, 1);
			glTranslatef(-px, -py, 0);
			L.draw(10, G.r,G.g,G.b);
			drawStringByPoint(L, "lowest point", 6, -3);
			drawAllPoints();
			glutSwapBuffers();
			
			//increment the angle and wait for a little while
			x += 0.5;
			wait(2000);
	    }
	    clear();

	    //show the outcome of the sorting by polar angle
	    L.draw(10, G.r,G.g,G.b);
	    drawStringByPoint(L, "lowest point", 6, -3);
	    drawAllPoints();
	    drawLinesAndLabels(L,80000);
	    displayMessage("Points sorted by polar angle", 70);
	    displayMessage("relative to the lowest point.", 20);
	    clear();
	    drawAllPoints();
	    glutSwapBuffers();
}

//shows a rotating line to indicate the sorting by polar angle relative to the current point
void showJarvisScan(Point& P, stack<Point> currentStack, colorName cName) {

	//start the angle at the most recent rotation value!!
    int degrees = Point::getRotation()*180 / pi;
    double x = degrees;
    double px = P.getX();
    double py = P.getY();
	
    //make the ray long enough to span the longest diagonal of the window
    double xEnd = dist4(0, xmax - xmin, 0, ymax - ymin);
    double yEnd = 0;
	
	//rotate all the way arround in 720 steps
    if(randomizeColors){
	cName=colors[5];
    }
    Color c(cName);
    while (x < 360 + degrees) {
		glColor3f(c.r, c.g, c.b);
		
		//redraw everything each time
		clear();
		
		//shift origin to point and rotate x degrees then draw line
		glTranslatef(px, py, 0);
		glRotatef(x, 0, 0, 1);
		glBegin(GL_LINES);
			glVertex2f(0, 0);
			glVertex2f(xEnd, yEnd);
		glEnd();
		
		//go back to the original orientation and draw the rest
		glRotatef(-x, 0, 0, 1);
		glTranslatef(-px, -py, 0);
		drawAllPoints();
		drawStack(currentStack);
		glutSwapBuffers();
		
		//increment the angle and wait briefly
		x += 0.5;
		wait(800);
    }
}


	//////////////////////////////////////////////////////////////////////////////////
	//						TEXT FUNCTIONS / UTILITIES								//
	//////////////////////////////////////////////////////////////////////////////////
	
	
//this function displays a message at the given offset from the lower right corner 
//the message morphs from C1 to C2 in 510 steps, waiting 'pauseLength' microseconds between color changes
void displayMessage(string s, int up, int right, int pauseLength, colorName C1, colorName C2) {

    //find the amount of change between the two colors in r, g and b values
    if(randomizeColors){
	C1=colors[2];
	C2=colors[1];
    }
    Color c1(C1);
    Color c2(C2);
    double deltaR = c1.r - c2.r;
    double deltaG = c1.g - c2.g;
    double deltaB = c1.b - c2.b;
    //cout<<"dr: "<<deltaR<<" dg: "<<deltaG<<" db: "<<deltaB<<endl;

	//create a color for each step and draw it, then redraw.
    for (int i = 0; i < 510; i++) {
		double r = c1.r - deltaR * i / 510.0;
		double g = c1.g - deltaG * i / 510.0;
		double b = c1.b - deltaB * i / 510.0;
		//cout<<"i: "<<i<<" r: "<<r<<" g: "<<g<<" b: "<<b<<endl;
		drawStringByPixel(right, up, s, GLUT_BITMAP_TIMES_ROMAN_24, r, g, b);
		wait(pauseLength);
		glutSwapBuffers();
    }
}
	
//performs the classic pythagorean distance calculation between two points
double dist4(double x1, double x2, double y1, double y2) {
    double X = x1 - x2;
    double Y = y1 - y2;
    return sqrt(X * X + Y * Y);
}
	
//draws a string relative to a given pair of pixels, with font and color optionaly set
void drawStringByPixel(int xPix, int yPix, string s, void* font, double r, double g, double b) {
	
	//orient the text relative to the pixels, not the model
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0.0, winWidth, 0.0, winHeight);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    //cout<<"R: "<<r<<" G: "<<g<<" B: "<<b<<endl;
    glColor3f(r, g, b); // Green by default

	//draw the text on the screen
    glRasterPos2i(xPix, yPix);
    for (string::iterator i = s.begin(); i != s.end(); ++i) {
	char c = *i;
	glutBitmapCharacter(font, c);
    }
	
	//return the matrix stack to its original condition
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
}

//draws a string relative to a point with some spacing, with font and color optionaly set
void drawStringByPoint(Point& p, string s, double xSpace, double ySpace, void* font, colorName cName) {
        if(randomizeColors){
	cName=colors[5];
	}
	Color c(cName);
    //find the pixel location of the point
    double xOffset = xmax - p.getX();
    double yOffset = ymax - p.getY();
    double xScale = winWidth / (xmax - xmin);
    double yScale = winHeight / (ymax - ymin);
    double xPix = (winWidth - xOffset * xScale) + xSpace; //a little space between point center and font start
    double yPix = (winHeight - yOffset * yScale) + ySpace;
	
	//draw the string by pixel location
    drawStringByPixel(xPix, yPix, s, font, c.r, c.g, c.b);

}

//converts an int to a string using stringstream
string intToString(int n) {
    std::string s;
    std::stringstream out;
    out << n;
    s = out.str();
    return s;
}

// true if the current vertices list contains a point, false otherwise
bool listContains(Point& p) {
    //cout << "checking";
    list<Point> vertices = cHull->getVertices();
    //cout << " with list size: " << vertices.size() << endl;
    list<Point>::iterator it = vertices.begin();
    list<Point>::iterator end = vertices.end();
    while (it != end) {
		if ((*it) == p) {
			return true;
		}
		it++;
    }
    return false;
}

colorName randomColor(){
    
      /* initialize random seed: */


  /* generate secret number: */
  int n = rand() % PERU + 1;
        switch (n){
            case BLANCHED_ALMOND:
                return BLANCHED_ALMOND;
                break;
            case DARK_SLATE_GREY:
                return DARK_SLATE_GREY;
                break;
            case WHITE:
                return WHITE;
                break;
            case GREEN:
                return GREEN;
                break;
            case YELLOW:
                return YELLOW;
                break;
            case MEDIUM_BLUE:
                return MEDIUM_BLUE;
                break;
            case BLUE:
                return BLUE;
                break;
            case RED:
                return RED;
                break;
            case GREEN_YELLOW:
                return GREEN_YELLOW;
                break;
            case BLACK:
                return BLACK;
                break;
            case ORANGE:
                return ORANGE;
                break;
            case TEAL:
                return TEAL;
                break;
            case KHAKI:
                return KHAKI;
                break;
            case INDIGO:
                return INDIGO;
                break;
            case MISTY_ROSE:
                return MISTY_ROSE;
                break;
            case OLIVE:
                return OLIVE;
                break;
	   case PERU:
                return PERU;
                break;
        }
}

//waits for the given number of microoseconds (approximately)
void wait(int ms) {


    unsigned long l = 0;
    struct timeval t;
    gettimeofday(&t, NULL);
    struct timeval tStart = t;

    //loop until the difference between old and new 
    //time is the given number of microseconds
    //or the number of loops exceeds the unsigned 
    //long int maximum (in case of an error (7 secs?))
    while (l < 4294967295) {
		gettimeofday(&t, NULL);
		//use seconds
		int diff = (t.tv_usec + 1000000 * t.tv_sec) - (tStart.tv_usec + tStart.tv_sec * 1000000);
		
		//cout<<"diff:"<<diff<<endl;
		if (diff >= ms)
			return;
		l++;
    }
    cerr << "the number of milliseconds: " << ms << " was too long to wait";
}

