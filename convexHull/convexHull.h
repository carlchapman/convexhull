// ************
// convexHull.h
// ************

#ifndef CONVEX_HULL_H
#define CONVEX_HULL_H 


#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <GLUT/glut.h>
#include <exception>
#include <list> 
#include <stack>
#include <sstream>


using namespace std; 


const double pi = 3.14159265358979323846; 
const double epsilon = 0.000001;  // less than epsion is zero
enum method {HULL_DISPLAY, GRAHAM_SCAN, JARVIS_MARCH,TOGGLE_GRID,TOGGLE_RANDOM_COLOR}; 


	//////////////////////////////////////////////////////////////////////////////////
	//							 	POINT											//
	//////////////////////////////////////////////////////////////////////////////////

	
class Point    
{
public: 
	Point(double a = 0, double b = 0): x(a), y(b) {}       
	Point(const Point& pt);                  // (1 pt)
	Point operator-() const;                 // (1 pt) negation
	Point operator-(const Point &q) const;   // (1 pt) vector subtraction
	double operator*(const Point &q) const;  // (1 pt) dot product 
	double operator%(const Point &q) const;  // (1 pt) cross product
	double norm() const;                     // (1 pt) distance from the origin to the point 
	Point unit_vector() const;	         // (1 pt) unit vector from the origin to the point
    double polarAngle(Point referencePt=Point(0,0)) const;    // (1 pt) polar angle of the point 
	// draw a point as a disk (see Section 2 of openGL101.pdf on how to do this)
	void draw(int size=5, float r = 1.0f, float g = 0.0f, float b = 0.0f) const; // (1 pt)
	void setXY(double xx, double yy);        // (1 pt) reset the x- & y-coordinate values 
	double getX()  const { return x; }
	double getY()  const { return y; }
	void print() const{ cout << "( " << x << ", " << y << " )" << endl; }
	
	//********************************************************************************
	//					FUNCTIONS ADDED TO POINT									//
	string toString() const;														//
	bool operator==(const Point& other) const;										//
	double cross(const Point& p2, const Point& p3); 								//
	// finds the cross product of the two vectors formed with this point for computational geometry
	Point static getNexus();					//gets the point used for operator< calculations
    void static setNexus(Point& newNexus);		//sets the point used for operator< calculations
    double static getRotation();				//gets the rotational offset (in radians) for polar angle calculations
    void static setRotation(double newRotation);//sets the rotational offset (in radians) for polar angle calculations
	friend double dist(const Point& a, const Point& b);  //dist between two points
    friend bool operator<(Point& a, Point& b);  //returns true if the polar angle of a is less than b
	//																				//
	//********************************************************************************
	
	
private: 
	double x, y; 								//the two point values
	//********************************************************************************
	//					STATIC VARIABLES ADDED TO POINT								//
	static Point nexus;							//the current point used for operator< calculations
    static double rotation;						//the current rotational offset used for polar angle calculations
	//																				//
	//********************************************************************************
}; 


	//////////////////////////////////////////////////////////////////////////////////
	//********************************************************************************
	//				EXCEPTIONS	(twoPointsEqual and badInput)						//
																					//
class twoPointsEqual : public exception {											//
public:																				//
																					//
    twoPointsEqual(Point& point1, Point& point2) : p1(point1), p2(point2) {}		//
    ~twoPointsEqual()throw () {}													//
    virtual const char* what() const throw ();										//
																					//
private:																			//
    Point p1, p2;																	//
};																					//
																					//
class badInput : public exception {													//
public:																				//
																					//
    badInput(string m) : message(m) {}												//
    ~badInput() throw () {}															//
    virtual const char* what() const throw () {return message.c_str();}				//
																					//
private:																			//
    string message;																	//
};																					//
//********************************************************************************  //
	//////////////////////////////////////////////////////////////////////////////////

	
	//////////////////////////////////////////////////////////////////////////////////
	//				CONVEX HULL BASE CLASS (ABSTRACT)								//
	//////////////////////////////////////////////////////////////////////////////////

	
class convexHull
{
public: 
    convexHull(int numPts, Point pts[]);         // (2 pts) constructor 
    int   getNumPoints();                        // (1 pt)
    void  getPoints(Point* pts);                 // (2 pts)
    Point getCurPoint();                         // (1 pt) get the point being processed during hull construction
    void  getTempVertices(stack<Point>& stk);    // (2 pts) get tempVertices for display
	virtual void constructHull() = 0;      // construct the convex hull. no graphics. 
    virtual void oneOp() = 0;              // execute one operation of Graham's scan or
					       // Javis' march.  In Graham scan, an
					       // operation is either push or pop. In
					       // Javis' march, it is the finding of the
					       // next vertex. 
	friend ostream& operator<< (ostream& ostr, convexHull *ch); 
	
	//********************************************************************************
	//				PUBLIC FUNCTIONS ADDED TO CONVEX HULL							//
	virtual list<Point>& getSortedList() = 0; //get the sorted list of the method	//
	bool finished() ;						// true if finished constructing		//
	list<Point>& getVertices();// gets a refference to a list of the vertices of the hull
	void reset(); // resets the boolean and tempVertices stack to prepare for another run
	//********************************************************************************
	
	
protected: 
    int numPoints;           // number of points 
	Point* points;           // point locations 

    int numVertices;         // number of vertices of the convex hull 
    list<Point> vertices;    // locations of vertices in counterclockwise
				 // order; may start at any vertex 

    Point curPoint;             // point being processed by an algorithm
    stack<Point> tempVertices;  // stack with points hopefully (in the case
				    // of Graham's scan) or surely (in the case of
				    // Javis march) to be vertices of the convex hull.  

    bool hullConstructed;    // true if it has been constructed; false otherwise. 
	
	//********************************************************************************
	//					PROTECTED FUNCTION ADDED									//
	int getMinIndex();																//
	//********************************************************************************
}; 






	//////////////////////////////////////////////////////////////////////////////////
	//							GRAHAM'S SCAN										//
	//////////////////////////////////////////////////////////////////////////////////
	// this derived class implements the Graham's scan, an O(n * log n) algorithm that
	// computes the convex hull of a set of n points.  


class GrahamScan : public convexHull
{
public: 
        GrahamScan(int numPts, Point pts[]);  // (3 pts) 
		method which() const;       // (1 pt) which algorithm is used 
        void constructHull();       // (16 pts) construct the convex hull without graphics
        void oneOp();               // (9 pts) execute one step of Graham's scan. this could be 
				    // be a push operation or a pop operation. used for
				    // graphics display. store the configuration after the
				    // operation in convexHull::tempVertices.
					
	//********************************************************************************
	//					PUBLIC FUNCTIONS ADDED TO GRAHAM'S SCAN						//
		~GrahamScan();																//
		Point& getLowestPoint();													//
		list<Point>& getSortedList();												//
	//********************************************************************************

private: 
	stack<Point> pointStack;    
        Point lowestPoint; 
        list<Point> *sortedList;    // list of remaining points ordered by polar angle for
				    // scanning. no need to implement a sorting algorithm by yourself. 
					
    //********************************************************************************
	//				CUSTUM PRIVATE MEMBERS FOR GRAHAM'S SCAN						//
	void init();																	//
	void complete();																//
    bool popping;																	//
    int counter;																	//
    list<Point>::iterator p3;														//
	//********************************************************************************
};





	//////////////////////////////////////////////////////////////////////////////////
	//							JARVIS' MARCH										//
	//////////////////////////////////////////////////////////////////////////////////
	// this derived class implements Javis march, an O(d*n) algorithm for convex hull
	// construction, where d is the number of vertices. 
	
	
class JarvisMarch : public convexHull
{
public: 
    JarvisMarch(int numPts, Point pts[]);  //  (3 pts) 
	method which() const;    // (1 pt) which algorithm is used 
	void constructHull();    // (13 pts) run Javis' march algorithm 
    void oneOp();            // (9 pts) execute one step which is to find the next vertex. 
	
	//********************************************************************************
	//					PUBLIC FUNCTIONS ADDED TO JARVIS' MARCH						//
	~JarvisMarch();																	//
	list<Point>& getSortedList();										//
	Point& getNextPoint();												//
	//********************************************************************************
	
private: 
        list<Point> *unprocessedList;
// unprocessed points sorted by their polar angles with respect to the last found vertex 
						 
    //********************************************************************************
	//				CUSTUM PRIVATE MEMBERS FOR JARVIS' MARCH						//
    Point firstPoint;																//
    int stepCounter;																//
	void init();																	//
	void complete();																//
	void getNextVertex();															//
	//********************************************************************************
}; 

	

#endif
